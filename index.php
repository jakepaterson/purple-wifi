<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <?php
                    //Function for accessing the RSS feed
                    function getFreshContent() {
                        $html = "";
                        //array for the RSS feeds 
                        $newsSource = array(
                            array(
                                "title" => "BBC",
                                "url" => "https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/2643123"
                            ),
                            array(
                                "title" => "Mett Office",
                                "url"   => "http://www.metoffice.gov.uk/public/data/PWSCache/WarningsRSS/Region/nw"
                            ),
                        );

                        //Function here for getting the RSS feeds displayed
                        function getFeed($url){
                            //Sets HTML variable
                            $html = "";

                            //Loads the files
                            $rss = simplexml_load_file($url);

                            //Sets the counter for the For loop
                            $count = 0;
                            //Append UL to the html variable
                            $html .= '<ul>';
                            foreach($rss->channel->item as$item) {
                                $count++;
                                if($count > 10){
                                    //exit the loop here
                                    break;
                                }
                                //append this to the HTML variable
                                $html .= '<li><a href="'.htmlspecialchars($item->link).'">'.htmlspecialchars($item->title).'</a></li>';
                            }
                            $html .= '</ul>';
                            return $html;
                        }

                        //For every News source give it a H2 for title and use the data from the RSS feed URL
                        foreach($newsSource as $source) {
                            $html .= '<h2>'.$source["title"].'</h2>';
                            $html .= getFeed($source["url"]);
                        }
                        return $html;
                    }
                ?>
                </div>
            </div>
        </div>
    </body>
</html>